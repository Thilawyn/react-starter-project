import dotenv from "dotenv"
import express from "express"
import path from "path"


const mode = process.env.NODE_ENV || "development"


dotenv.config()

const httpHost = process.env.HTTP_HOST || ""

const httpPort = Number(process.env.HTTP_PORT)
if (!Number.isInteger(httpPort)) throw "HTTP_PORT env variable must be an integer"


const app = express()

async function registerRoutes() {
    if (mode === "production")
        app.get("/webapp.js", (_, res) => {
            res.sendFile( path.join(process.cwd(), "dist/webapp.js") )
        })

    if (mode === "development") {
        const { webpackDevServer } = await import("./webpack-dev-server")
        app.use(webpackDevServer)
    }

    app.get("*", (_, res) => {
        res.sendFile( path.join(process.cwd(), "index.html") )
    })
}

registerRoutes().then(() => {
    app.listen(httpPort, httpHost, 511, () => {
        console.log("HTTP listening on port " + httpPort + ".")
    })
})
