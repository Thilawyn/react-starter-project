import { Router } from "express"
import { webpack } from "webpack"
import webpackDevMiddleware from "webpack-dev-middleware"
import webpackHotMiddleware from "webpack-hot-middleware"
// @ts-ignore
import webpackConfig from "../webpack.config.js"


export const webpackDevServer = Router()
const webpackCompiler = webpack(webpackConfig)

webpackDevServer.use( webpackDevMiddleware(webpackCompiler, {}) )
webpackDevServer.use( webpackHotMiddleware(webpackCompiler, {}) )
