import { createTheme, CssBaseline, ThemeProvider, Typography } from "@mui/material"
import React from "react"


const theme = createTheme({

})


export default function WebApp() {

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />

            <Typography variant="h1">It works!</Typography>
        </ThemeProvider>
    )

}
